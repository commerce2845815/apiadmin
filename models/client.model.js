const mongoose = require('mongoose')

const clientSchema = new mongoose.Schema({
    enseigneCommercial: {
        type: String,
        // required: true,
    },
    companyAdress: {
        type: String,
        // required: true
    },
    companyWebsite: {
        type: String,
        // require: true
    },
    googleBusiness: {
        type: String,
        // required: true,
    },
    googleStars: {
        type: String,
        // required: true,
    },

    description: {
        type: String,
        // required: true,
    },
    about_company: {
        activity: {
            type: String,
            // required: true,
        },
        workForce: {
            type: String,
            // required: true,
        },
        collaborationStartDate: {
            type: String,
            // required: true,
        },
        history: {
            type: String,
            // required: true,
        },
    },
    contact: {
        fullName: {
            type: String,
            // required: true,
        },
        telephone: {
            type: Number,
            // required: true,
        },
        email: {
            type: String,
            // required: true,
        },
    },
    billing_infos: {
        bankInfos: {
            type: String,
            // required: true,
        },
        paymentTerms: {
            type: String,
            // required: true,
        },
        paymentHistory: {
            type: String,
            // required: true,
        },
    },

    notes: {
        type: String,
        // required: true,
    },
    additionNotes: {
        type: String,
        // required: true,
    },

},
    { timestamps: true })



module.exports = Client = mongoose.model('client', clientSchema)