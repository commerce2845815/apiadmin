const Client = require('../models/client.model');
const { validationResult } = require('express-validator')

const ajouterClient = async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res
            .status(400)
            .json({ errors: errors.array({ onlyFirstError: true }) })
    }
    try {

        const { enseigneCommercial, companyAdress, companyWebsite, googleBusiness, googleStars, description,
            about_company: {
                activity,
                workForce,
                collaborationStartDate,
                history,
            },
            contact: {
                fullName,
                email,
                telephone,
            },
            billing_infos: {
                bankInfos,
                paymentTerms,
                paymentHistory,
            }, notes, additionNotes } = req.body
        console.log("req", req.body);
        const nouveauClient = new Client({
            enseigneCommercial,
            companyAdress,
            companyWebsite,
            googleBusiness,
            googleStars,
            description,
            about_company: {
                activity,
                workForce,
                collaborationStartDate,
                history,
            },
            contact: {
                fullName,
                email,
                telephone,
            },
            billing_infos: {
                bankInfos,
                paymentTerms,
                paymentHistory,
            },
            notes,
            additionNotes
        });
        console.log(nouveauClient);

        await nouveauClient.save();
        res.status(200).send({ msg: "client ajouté avec succès" })

    } catch (error) {
        throw new Error('Erreur lors de l\'ajout du client : ' + error.message);
    }
}
const afficherClients = async (req, res) => {
    try {
        const clients = await Client.find();
        res.status(200).json(clients);
    } catch (error) {
        res.status(500).json({ msg: 'Erreur lors de la récupération des clients : ' + error.message });
    }
}
const modifierClient = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array({ onlyFirstError: true }) });
        }

        const clientId = req.params.id;
        const clientData = req.body;

        const clientModifie = await Client.findByIdAndUpdate(clientId, clientData, { new: true });

        if (!clientModifie) {
            return res.status(404).json({ msg: "Client non trouvé" });
        }

        res.status(200).json({ msg: "Client modifié avec succès", client: clientModifie });
    } catch (error) {
        res.status(500).json({ msg: 'Erreur lors de la modification du client : ' + error.message });
    }
};
module.exports = { ajouterClient, afficherClients, modifierClient }