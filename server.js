const express = require('express')
const mongoose = require('mongoose')
require("dotenv").config()
const cors = require('cors')

const app = express()
app.use(cors()) 

// Middleware pour analyser les corps de requête JSON
app.use(express.json())


mongoose.connect(`mongodb://localhost:27017/client`)
    .then(() => {
        console.log("database connected");
    })
    .catch(() => {
        console.log("error connected");
    })

const port = process.env.PORT
app.listen(5000, () => {
    console.log(`connected at ${port}`);
})

app.use('/client', require('./routers/client.router'))
