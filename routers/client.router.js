const express = require('express');
const { ajouterClient ,afficherClients, modifierClient} = require('../controllers/client.controller');
const router = express.Router()


router.post('/form', ajouterClient)
router.get('/', afficherClients)
router.put('/modifier/:id', modifierClient)

module.exports = router;